package com.psyt.ballbounce;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.View;

/**
 * AnimatedBall Class for demonstrating effects of plot on mind thinking
 * Child class of View
 * @author SeRoAl
 * @version 0.1.0
 * @see android.view.View
 */
public class AnimatedBall extends View{

	private Context mContext;
	float x = -1;           // Holds x position of ball
	float y = -1;           // Holds y position of ball
	private float upVelocity = (float)Math.PI/100;          // Up going velocity
    private float downVelocity = (float)Math.PI/40;         // Down going velocity
    private int ballSize = 25;      // Size of Ball
    private int ballSizeRatio = 40; // Ratio of ball radius to lowest screen dimension(height,width)
    private float yMin = 0;         // Holds min bound of y
    private float yMax = 0;         // Holds max bound of y
    private boolean isInitiated = false;        // Show whether variables initiated or not
    private float theta= (float)Math.PI;        // Angle of ball[ y is sinusoidal projection of angle]
	private Handler h;                          // Handler for refreshing of view
    private int frameRate = 50;                 // Refresh rate [ms]
    private boolean hasPlot = true;             // Enable/Disable sinusoidal plot
    private float velocity = 0;                 // Current velocity of ball

    /**
     *  Class constructor
     * @param context
     * @param attrs
     */
	public AnimatedBall(Context context, AttributeSet attrs)  {
		super(context, attrs);  
		mContext = context;  
		h = new Handler();
    }

    /**
     * Returns Ball radius ratio
     * @return Ball ratio
     */
    public int getBallSizeRatio() {
        return ballSizeRatio;
    }

    /**
     * Sets Ball radius ratio
     * @param ballSizeRatio
     */
    public void setBallSizeRatio(int ballSizeRatio) {
        if (ballSizeRatio>0)
            this.ballSizeRatio = ballSizeRatio;
    }

    /**
     * Returns frame rate
     * @return frame rate
     */
    public int getFrameRate() {
        return frameRate;
    }

    /**
     * Sets frame rate
     * @param frameRateMillisecond
     */
    public void setFrameRate(int frameRateMillisecond) {
        if (frameRateMillisecond>0)
            this.frameRate = frameRateMillisecond;
    }

    /**
     * Initialize variables
     * @param forceInit if true enforces to re-init variables
     */
    public void initVariables(boolean forceInit){
        if (forceInit || (!isInitiated)) {
            x = this.getWidth() / 2;
            ballSize = Math.min(this.getWidth(), this.getHeight()) / ballSizeRatio;
            y = ballSize;
            yMin = ballSize;
            yMax = this.getHeight() - ballSize;
        }
    }

    /**
     * Sets up time of bouncing ball
     * @param upTime in ms
     */
    public void setUpTime(float upTime){
        if (upTime>0)
            upVelocity = 2*(float)Math.PI*frameRate/upTime;
    }

    /**
     * Sets down time of bouncing ball
     * @param downTime in ms
     */
    public void setDownTime(float downTime){
        if (downTime>0)
            downVelocity =  2*(float)Math.PI*frameRate/downTime;
    }

    /**
     * Returns Up going time of bouncing ball
     * @return time
     */
    public float getUpTime(){
        return (2*(float)Math.PI*frameRate)/(upVelocity);
    }

    /**
     * Returns Down going time of bouncing ball
     * @return time
     */
    public float getDownTime(){
        return (2*(float)Math.PI*frameRate)/(downVelocity);
    }

    /**
     * Returns current velocity of ball
     * Note that the value is absolute value
     * @return velocity
     */
    public float getVelocity() {
        return velocity;
    }

    /**
     * Indicates that the view has plot or not
     * @return hasPlot
     */
    public boolean hasPlot() {
        return hasPlot;
    }

    /**
     * Sets the state of showing plot
     * @param hasPlot
     */
    public void setHasPlot(boolean hasPlot) {
        this.hasPlot = hasPlot;
    }

    /**
     * Runnable to refresh the view
     */
    private Runnable r = new Runnable() {
		@Override
		public void run() {
			invalidate(); 
		}
	};

    /**
     * Returns path of plot for the half of screen after ball
     * @return path
     */
    public Path getAfterPath(){
        float newTheta = theta;
        float velocityTemp = 0;
        float x1=x;
        Path path= new Path();
        path.moveTo(x,y);

        //
        do{
            path.lineTo(x1, (FloatMath.sin(newTheta) * (yMax - yMin) / 2) + ((yMax - yMin) / 2) + yMin);
            //
            velocityTemp = getVelocity(newTheta,velocityTemp);

            newTheta += (float) Math.PI/100;
            if (newTheta > (2 * ((float) Math.PI)))
                newTheta -= 2 * ((float) Math.PI);


            x1+= (this.getWidth() *(float)Math.PI)/(36000*velocityTemp);

        }while (x1<this.getWidth());

        //
        return path;
    }

    /**
     * Returns path of plot for the half of screen before ball
     * @return path
     */
    public Path getBeforePath(){
        float newTheta = theta;
        float velocityTemp = 0;
        float x1=x;
        Path path= new Path();
        path.moveTo(x,y);

        //
        do{
            path.lineTo(x1,(FloatMath.sin(newTheta) * (yMax - yMin) / 2) + ((yMax - yMin) / 2) + yMin);
            //
            velocityTemp = getVelocity(newTheta,velocityTemp);
            //
            newTheta -= (float) Math.PI/100;
            if (newTheta < -(2 * ((float) Math.PI)))
                newTheta += 2 * ((float) Math.PI);

            x1-= (this.getWidth() *(float)Math.PI)/(36000*velocityTemp);

        }while (x1>0);

        return path;
    }

    /**
     * Returns next velocity of bouncing ball based on current position of ball(angle and speed)
     * @param angle
     * @param velocityBefore
     * @return nextVelocity
     */
    private float getVelocity(float angle, float velocityBefore){
        float nextVelocity = velocityBefore;
        //
        if (FloatMath.cos(angle)>= 0) {
            nextVelocity = downVelocity;
            if (FloatMath.cos(angle + velocityBefore) < 0)
                nextVelocity = upVelocity;
        } else if (FloatMath.cos(angle)< 0) {
            nextVelocity = upVelocity;
            if (FloatMath.cos(angle + velocityBefore) > 0)
                nextVelocity = downVelocity;
        }
        return nextVelocity;
    }


    /**
     * Draw the view
     * @param c
     */
	protected void onDraw(Canvas c) {
        initVariables(false);
        //
        velocity = getVelocity(theta,velocity);
        theta+=velocity;
        if (theta>(2*((float)Math.PI)))
            theta -= 2*((float)Math.PI);

        Paint p = new Paint();
        p.setStyle(Paint.Style.STROKE);
        p.setColor(Color.BLUE);
        p.setStrokeWidth(5);
        //
        y = (FloatMath.sin(theta)*(yMax-yMin)/2)+((yMax-yMin)/2)+ yMin;
        //
        c.drawCircle(x, y, ballSize, p);
        //
        if (hasPlot) {
            p.setStyle(Paint.Style.STROKE);
            p.setColor(Color.BLACK);
            p.setAntiAlias(true);
            c.drawPath(getAfterPath(), p);
            c.drawPath(getBeforePath(), p);
        }
        //
        /*
        p.setColor(Color.WHITE);
        p.setStyle(Paint.Style.FILL);

        c.drawRect(10,10,500,200,p);
        p.setStyle(Paint.Style.FILL);
        p.setColor(Color.BLUE);
        p.setTextSize(40);
        c.drawText("Y:"+y+" - S:"+ballSize,20,100,p);
        */

	    //c.drawBitmap(ball.getBitmap(), x, y, null);
	    
	    h.postDelayed(r, frameRate);
	}
}
