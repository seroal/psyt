package com.psyt.ballbounce;

import android.graphics.Color;
import android.os.Bundle;
import android.app.Activity;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Main activity class
 * @author SeRoAl
 * @version 0.1.0
 * @see android.app.Activity
 */
public class MainActivity extends Activity {
    // Variables for managing components
    SeekBar seekBarUp, seekBarDown;
    TextView upText,downText;
    CheckBox hasPlot;
    AnimatedBall aBall;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //
        initVariables();
    }

    /**
     * Initializes variables
     */
    private void initVariables(){
        aBall = (AnimatedBall) findViewById(R.id.anim_view);
        seekBarUp = (SeekBar)findViewById(R.id.upSpeed);
        seekBarDown = (SeekBar)findViewById(R.id.downSpeed);
        //
        upText = (TextView)findViewById(R.id.upSpeedText);
        upText.setTextColor(Color.BLACK);
        downText = (TextView)findViewById(R.id.downSpeedText);
        downText.setTextColor(Color.BLACK);
        //
        hasPlot = (CheckBox)findViewById(R.id.hasPlot);
        hasPlot.setTextColor(Color.BLACK);
        //
        aBall.setDownTime(3000);
        aBall.setUpTime(3000);
        //
        upText.setText(String.format("%.1f",3.0f));
        downText.setText(String.format("%.1f", 3.0f));
        //
        hasPlot.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                aBall.setHasPlot(isChecked);
            }
        });
        //
        seekBarUp.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Float val = (12.00f-3.00f)*(float)progress/seekBar.getMax() + 3.0f;
                //
                upText.setText(String.format("%.1f",val));
                //
                aBall.setUpTime(val*1000);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBarDown.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Float val = (12.00f-3.00f)*(float)progress/seekBar.getMax() + 3.0f;
                //
                downText.setText(String.format("%.1f", val));
                //
                aBall.setDownTime(val * 1000);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }
}
